
import VueRouter from 'vue-router'

let routes = [
    {
        path: '/',
        component: require('./views/admin/index')
    },
    {
        path: '/foo',
        component: require('./views/admin/somethingelse')
    }
];

export default new VueRouter({
    routes
});