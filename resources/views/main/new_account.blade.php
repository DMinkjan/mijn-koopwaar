@extends('main.template.master')

@section('content')
	<div class="content-box content-box-100">
		<h1>Mijn koopwaar <span>mijn Account</span></h1>
		<p>Beheer uw advertenties, biedingen en bewaarde advertenties.</p>
	</div>

	<div class="content-box content-box-50 loginbox">
		<h3>Maak een account aan</h3>
		<p>Vul uw volledige naam en emailadres in om uw account aan te maken.</p>
		{!! Form::open(['class' => 'form-horizontal']) !!}
			<div class="input-row">
				{!! Form::label('name', 'Uw naam', ['class' => 'input-label']) !!}

				{!! Form::text('name', NULL, ['class' => 'form-input', 'required' => true]) !!}
			</div>

			<div class="input-row">
				{!! Form::label('email', 'Uw email', ['class' => 'input-label']) !!}

				{!! Form::text('email', NULL, ['class' => 'form-input', 'required' => true]) !!}
			</div>

			<div class="input-row">
				{!! Form::checkbox('general_terms', '1') !!}Ja, ik ga akkoord met de <a href="#">Algemene voorwaarden</a>
			</div>

			<div class="input-row">
				{!! Form::submit('Maak account aan', ['class' => 'button button-green']) !!}
			</div>
		{!! Form::close() !!}
	</div>

	<div class="content-box content-box-50 loginbox align-right">
		<h2>Inloggen</h2>
		
		{!! Form::open(['class' => 'main_login_form form-horizontal']) !!}
			<div class="input-row">
				{!! Form::label('email', 'Emailadres', ['class' => 'input-label']) !!}

				{!! Form::text('email', NULL, ['class' => 'form-input', 'required' => true]) !!}
			</div>

			<div class="input-row">
				{!! Form::label('password', 'Wachtwoord', ['class' => 'input-label']) !!}

				{!! Form::password('password', ['class' => 'form-input']) !!}

				<p>
					<a href="">(Wachtwoord vergeten?)</a>
				</p>
			</div>
			<div class="input-row">
				{!! Form::submit('Inloggen', ['class' => 'button button-green']) !!}
			</div>
		{!! Form::close() !!}
	</div>
@endsection