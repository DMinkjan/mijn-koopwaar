@define($image = $advertisement->image())
@if(is_null($image))
	@define($image = "dummy.jpg")
@else
	@define($image = $advertisement->image()->image_name)
@endif
<li>
	<a href="">
		<img src="{{ url('advertisements/'.$image)}}" class="small_advert_thumb" />
		<span class="advert_text">
			<strong>{{ $advertisement->title }}</strong>
			<span class="price">
				&euro; {{ $advertisement->price }}
			</span>
		</span>
	</a>
</li>