@extends('main.template.master')

@section('content')
	<div class="content-box content-box-100">
		<h1>Mijn koopwaar <span>mijn Account</span></h1>
		<p>Beheer uw advertenties, biedingen en bewaarde advertenties.</p>
	</div>

	<div class="content-box content-box-50 loginbox">
		<h3>Maak een account aan</h3>
		<p>Om gebruik te maken van Mijnkoopwaar heeft u een account nodig. Hiermee kunt u voortaan:</p>
		<ul>
			<li>Advertenties plaatsen</li>
			<li>Uw Koopwaar beheren</li>
			<li>Uw bod op andermans Koopwaar beheren</li>
			<li>Bewaarde advertenties beheren</li>
		</ul>
		<p><a href="{{ route('main.my_account.new_account') }}" class="new"><span>Maak account aan</span></a></p>
	</div>

	<div class="content-box content-box-50 loginbox align-right">
		<h2>Inloggen</h2>
		
		{!! Form::open(['class' => 'main_login_form form-horizontal']) !!}
			<div class="input-row">
				{!! Form::label('email', 'Emailadres', ['class' => 'input-label']) !!}

				{!! Form::text('email', NULL, ['class' => 'form-input', 'required' => true]) !!}
			</div>

			<div class="input-row">
				{!! Form::label('password', 'Wachtwoord', ['class' => 'input-label']) !!}

				{!! Form::password('password', ['class' => 'form-input']) !!}

				<p>
					<a href="">(Wachtwoord vergeten?)</a>
				</p>
			</div>
			<div class="input-row">
				{!! Form::submit('Inloggen', ['class' => 'button button-green']) !!}
			</div>
		{!! Form::close() !!}
	</div>
@endsection