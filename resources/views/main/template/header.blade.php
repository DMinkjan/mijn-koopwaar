<!DOCTYPE html>
<html>
<head>
	<title>MijnKoopwaar.nl</title>

	<link rel="stylesheet" type="text/css" href="{{ url('css/main.css') }}">
</head>
<body>
	<div id="wrapper">
		<div class="main_content">
			<div class="header_content">
				<div class="logo">
					<a href="">
						<img src="{{ url('img/logo.png') }}" />
					</a>
				</div>

				<div class="small_menu">
					<ul>
						<li><a href="">Home</a></li>
						<li><a href="">Help en contact</a></li>
						<li><a href="">Veilig handelen</a></li>
						<li><a href="">Inloggen</a></li>
					</ul>
				</div>
				
				<div class="main_menu">
					<ul class="tabs">
						<li class="categories">
							<span>
								<a href="#">Rubrieken</a>
							</span>
						</li>

						<li class="search">
							<span>
								<a href="#">Zoeken</a>
							</span>
						</li>

						<li class="advertisement">
							<span>
								<a href="#">Plaats advertentie</a>
							</span>
						</li>

						<li class="account">
							<span>
								<a href="">Mijn account</a>
							</span>
						</li>
					</ul>

					<div class="rubrics_menu">
						<div class="top">
							<div class="bottom">
								<div class="categories left">
									<ul>
										<li><a href="">Voorbeeld</a></li>
									</ul>
								</div>

								<div class="categories right">
									<ul>
										<li><a href="">Voorbeeld</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<div class="clear"></div>

				</div>
			</div>

			<div class="middle_content">
				<div class="left_content">
					@include('main.template.side_advertisements')
				</div>
				<div class="custom_content">
					@yield('content')
				</div>
			</div>
		</div>
	</div>