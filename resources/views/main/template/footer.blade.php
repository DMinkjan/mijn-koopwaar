<div class="footer_content">
	<div class="center">
		<div class="right">
			<p>&copy; Copyright {{ Date('Y') }}</p>
			
			<a href="">
				<img src="" />
			</a>
		</div>

		<ul>
			<li><a href="">Home</a></li>
			<li><a href="#">Help en contact</a></li>
			<li><a href="#">Algemene voorwaarden</a></li>
			<li><a href="#">Veilig handelen</a></li>
		</ul>

		<ul class="link">
			<li><a href="#">Zoeken</a></li>
			<li><a href="#" class="advertisement">Plaats een advertentie</a></li>
			<li><a href="" class="account">Mijn account</a></li>
		</ul>
	</div>
</div>

<!-- SCRIPTS -->
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script src="{{ url('js/main.js') }}"></script>
</body>
</html>