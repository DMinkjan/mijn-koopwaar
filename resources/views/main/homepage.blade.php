@extends('main.template.master')

@section('content')
	<div class="content-box content-box-50">
		<div class="advertisements-list">
			<h2>Top advertenties <span>op MijnKoopwaar</span></h2>
		</div>

		<div class="advertisements-small">
			<ul>
				<li>
					<a href="">
						<img src="{{ url('img/no_image.jpg')}}" class="small_advert_thumb" />
						<span class="advert_text">
							<strong>test</strong>
							<span class="price">
								&euro; 1245
							</span>
						</span>
					</a>
				</li>
				<li>
					<a href="">
						<img src="{{ url('img/no_image.jpg')}}" class="small_advert_thumb" />
						<span class="advert_text">
							<strong>test</strong>
							<span class="price">
								&euro; 1245
							</span>
						</span>
					</a>
				</li>
				<li>
					<a href="">
						<img src="{{ url('img/no_image.jpg')}}" class="small_advert_thumb" />
						<span class="advert_text">
							<strong>test</strong>
							<span class="price">
								&euro; 1245
							</span>
						</span>
					</a>
				</li>
				<li>
					<a href="">
						<img src="{{ url('img/no_image.jpg')}}" class="small_advert_thumb" />
						<span class="advert_text">
							<strong>test</strong>
							<span class="price">
								&euro; 1245
							</span>
						</span>
					</a>
				</li>
				{{--@foreach($top_advertisements as $advertisement)
					@include('main.components.advert_small')
				@endforeach--}}
			</ul>
		</div>
	</div>

	<div class="content-box content-box-50">
		<h2>Meest bekeken <span>op MijnKoopwaar</span></h2>

		<div class="thumb-galery">
			<ul>
				<li>
					<a href="#">
						<img src="{{ url('img/no_image.jpg') }}" alt="">
					</a>
				</li>
				{{--@foreach($top_viewed as $advertisement)
					@include('main.components.advertisement_thumb_small')
				@endforeach--}}
				<li>
					<a href="#">
						<img src="{{ url('img/no_image.jpg') }}" alt="">
					</a>
				</li>
			</ul>
		</div>
	</div>
@endsection