<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Administrator</title>

        <link href="css/app.css" rel="stylesheet">
    </head>
    <body>
        <div id="app">

            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <router-link class="navbar-brand" to="/">
                            <img alt="Mijn Koopwaar" src="{{ url('img/logo_png.png')}}" height="25px">
                        </router-link exact>
                    </div>
                </div>
            </nav>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3 col-md-2 sidebar">
                        <ul class="nav nav-sidebar">
                            <router-link tag='li' to="/" active-class="active" exact>
                                <a>home</a>
                            </router-link>
                            <router-link tag='li' to="/foo" active-class="active">
                                <a>test</a>
                            </router-link>
                        </ul>
                    </div>
                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                        <router-view></router-view>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <script src="js/manifest.js"></script>
    <script src="js/vendor.js"></script>
    <script src="js/app.js"></script>
</html>